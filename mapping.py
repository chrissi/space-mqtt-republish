#!/usr/bin/env python3

def makeNow(value):
    """
    Takes a byte-array and re-formates the value as collectd value.
    The value is marked to be <now> without further timestamp.
    """
    return "N:{}".format(value.decode())

# mapping is a list of tupels with the mappings to do:
#
# old_topic, new_topic, callable

mapping = [
    ("ispindel/iSpindel_S0/tilt",        "collectd/iSpindelS0/tilt/gauge", makeNow),
    ("ispindel/iSpindel_S0/temperature", "collectd/iSpindelS0/temperature/temperature", makeNow),
    ("ispindel/iSpindel_S0/battery",     "collectd/iSpindelS0/battery/gauge", makeNow),
    ("ispindel/iSpindel_S0/gravity",     "collectd/iSpindelS0/gravity/gauge", makeNow),
    ("ispindel/iSpindel_S0/RSSI",        "collectd/iSpindelS0/RSSI/gauge", makeNow)
]
